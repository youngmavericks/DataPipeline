from prefect import task, Flow, Parameter
from prefect.tasks.shell import ShellTask
import prefect
import os
import tweepy
import json
import subprocess


@task
def authenticate_api(consumerKey, consumerSecret, accessToken, acessTokenSecret):    
    # Authenticate twitter api
    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, acessTokenSecret)
    
    # Return authenticated API object
    return tweepy.API(auth)


@task
def get_locations(api):
    return api.trends_available()


@task
def structure_data(locations):
    return {x['name']: x['woeid'] for x in locations}


@task
def get_indexname(saveIndexName):
    return f"{saveIndexName}.json"


@task
def load_index(saveIndexLocation):
    saveIndexDict = {}

    if os.path.isfile(saveIndexLocation):
        with open(saveIndexLocation) as inputFile:
            saveIndexDict = json.load(inputFile)
    
    return saveIndexDict


@task
def save_name(fileName, outputLocation, saveIndexDict):
    saveIndexDict[fileName] = outputLocation
    return saveIndexDict

@task
def save_index(saveIndexDict, saveIndexLocation):
    with open(saveIndexLocation, 'w') as outputFile:
        json.dump(saveIndexDict, outputFile)


@task
def get_filename(saveLocation, fileName):
    return f"{saveLocation}/{fileName}.json"


@task
def save_data(outputLocation, structuredLocations):
    with open(outputLocation, 'w') as outputFile:
        json.dump(structuredLocations, outputFile)


with Flow("TwitterLocations") as flow:
    # Parameters
    consumerKey = Parameter("consumerKey")
    consumerSecret = Parameter("consumerSecret")
    accessToken = Parameter("accessToken")
    acessTokenSecret = Parameter("acessTokenSecret")
    saveIndexName = Parameter("saveIndex")
    saveLocation = Parameter("saveLocation")
    fileName = Parameter("fileName")

    logger = prefect.context.get("logger")

    # Flow
    api = authenticate_api(consumerKey, consumerSecret, accessToken, acessTokenSecret)
    saveIndexLocation = get_indexname(saveIndexName)

    saveIndexDict = load_index(saveIndexLocation)
    locations = get_locations(api)
    structuredLocations = structure_data(locations)

    outputLocation = get_filename(saveLocation, fileName)
    saveIndexDict = save_name(fileName, outputLocation, saveIndexDict)
    
    save_index(saveIndexDict, saveIndexLocation)
    save_data(outputLocation, structuredLocations)


def data_pipeline():
    # Load variables from env file into python environment

    input = {
        "consumerKey": os.getenv("CONSUMER_KEY"),
        "consumerSecret": os.getenv("CONSUMER_SECRET"),
        "accessToken": os.getenv("ACCESS_TOKEN"),
        "acessTokenSecret":  os.getenv("ACCESS_TOKEN_SECRET"),
        "saveIndex":  os.getenv("SAVE_INDEX"),
        "saveLocation":  os.getenv("SAVE_LOCATION"),
        "fileName":  os.getenv("TWITTER_COUNTRY_LOCATIONS_OUTPUT")
    }

    flow.run(parameters=input)


if __name__ == "__main__":
    data_pipeline()