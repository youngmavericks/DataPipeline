from prefect import task, Flow, Parameter, unmapped
from prefect.schedules import clocks, Schedule
import prefect
from datetime import date 
import spacy
import matplotlib.pyplot as plt
import re
from spacy.language import Language
import gensim
import gensim.corpora as corpora
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
import os
import json
import datetime

now = datetime.datetime.utcnow() + datetime.timedelta(minutes=6)

clock = clocks.IntervalClock(
    start_date=now,
    interval=datetime.timedelta(days=1)
)

schedule = Schedule(
    clocks=[clock]
)


@task
def get_latest_tweets_filename(dataFileName):
    return f"{dataFileName}{date.today()}"


@task
def get_indexname(saveIndexName):
    return f"{saveIndexName}.json"


@task
def load_index(saveIndexLocation):
    saveIndexDict = {}

    if os.path.isfile(saveIndexLocation):
        with open(saveIndexLocation) as inputFile:
            saveIndexDict = json.load(inputFile)
    
    return saveIndexDict


@task
def get_filename_index(saveIndexDict, fileName):
    return saveIndexDict[fileName]


@task
def load_data_file(dataFilePath, logger):
    if os.path.isfile(dataFilePath):
        with open(dataFilePath) as inputFile:
            trendingTweets = json.load(inputFile)

        return trendingTweets
    
    else:
        logger.warning(f"{locationFilePath} Does not exist. Please run trendingTweets data flow first")


@task
def load_nlp(pipelineName):
    return spacy.load(pipelineName)


@task
def clean_tweet(tweet):
    return re.sub(r"\s+", " ", re.sub(r"(\n|#)", " ", re.sub(r"http?s(\S+)", "", re.sub(r"@(\w+)", "", tweet)))).strip()


@task
def flatten_data(rawTrendingTweets):
    return [sen for k,v in rawTrendingTweets.items() for sen in v]


@Language.component("stopwords")
def remove_stopwords(doc):
    doc = [token.text for token in doc if token.is_stop != True and token.is_punct != True]
    return doc


@task
def add_stopwords(nlp):
    nlp.add_pipe("stopwords", last=True)

    return nlp


@task
def transform_data(nlp, cleanedTrendingTweets):
    return nlp.pipe(cleanedTrendingTweets, disable=['parser', 'entity_linker', 'entity_ruler', 'attribute_ruler', 'tok2vec', 'transformer'])


@task
def unpack_tweets(transformedTrendingTweets):
    return [sen for sen in transformedTrendingTweets]


@task
def create_mapping(trendingTweets):
    # Creates, which is a mapping of word IDs to words.
    return corpora.Dictionary(trendingTweets)


@task
def doc_to_bow(doc, mappingTweets):
    return mappingTweets.doc2bow(doc)


@task
def train_model(corpusTweets, mappingTweets):
    return gensim.models.ldamodel.LdaModel(corpus=corpusTweets,
                                           id2word=mappingTweets,
                                           num_topics=10, 
                                           random_state=42,
                                           update_every=1,
                                           passes=10,
                                           alpha='auto',
                                           per_word_topics=True)


@task
def create_visualization(ldaModel, corpusTweets, mappingTweets):
    return gensimvis.prepare(ldaModel, corpusTweets, mappingTweets)


@task
def save_html(output, saveName):
    pyLDAvis.save_html(output, saveName)


with Flow("TweetTopics") as flow:
    logger = prefect.context.get("logger")

    # Parameters
    saveIndexName = Parameter("saveIndex")
    dataFileName = Parameter("locationFileName")
    pipelineName = Parameter("pipelineName")
    saveName = Parameter("saveName")

    # Flow
    nlp = load_nlp(pipelineName)
    nlp = add_stopwords(nlp)

    saveIndexLocation = get_indexname(saveIndexName)
    saveIndexDict = load_index(saveIndexLocation)
    currentDataFilename = get_latest_tweets_filename(dataFileName)

    dataFilePath = get_filename_index(saveIndexDict, currentDataFilename)
    rawTrendingTweets = load_data_file(dataFilePath, logger)
    flattenedTrendingTweets = flatten_data(rawTrendingTweets)

    cleanedTrendingTweets = clean_tweet.map(tweet=flattenedTrendingTweets)

    transformedTrendingTweets = transform_data(nlp, cleanedTrendingTweets)
    trendingTweets = unpack_tweets(transformedTrendingTweets)

    mappingTweets = create_mapping(trendingTweets)
    corpusTweets = doc_to_bow.map(doc=trendingTweets, mappingTweets=unmapped(mappingTweets))
    ldaModel = train_model(corpusTweets, mappingTweets)

    output = create_visualization(ldaModel, corpusTweets, mappingTweets)
    save_html(output, saveName)


def data_pipeline(schedule):
    input = {
        "saveIndex":  os.getenv("SAVE_INDEX"),
        "locationFileName":  os.getenv("TRENDING_TWEETS_OUTPUT"),
        "pipelineName":  os.getenv("SPACY_NLP_PIPELINE"),
        "saveName": os.getenv("TWEET_TOPICS_OUTPUT")
    }

    flow.schedule = schedule
    flow.run(parameters=input)


if __name__ == "__main__":
    data_pipeline(schedule)