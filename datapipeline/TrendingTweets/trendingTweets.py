from prefect import task, Flow, Parameter, unmapped
from prefect.schedules import clocks, Schedule
import prefect
import os
import tweepy
import json
import subprocess
from datetime import date 
import datetime

now = datetime.datetime.utcnow() + datetime.timedelta(minutes=2)

clock = clocks.IntervalClock(
    start_date=now,
    interval=datetime.timedelta(days=1)
)

schedule = Schedule(
    clocks=[clock]
)

@task
def authenticate_api(consumerKey, consumerSecret, accessToken, acessTokenSecret):    
    # Authenticate twitter api
    auth = tweepy.OAuthHandler(consumerKey, consumerSecret)
    auth.set_access_token(accessToken, acessTokenSecret)
    
    # Return authenticated API object
    return tweepy.API(auth)


@task
def get_indexname(saveIndexName):
    return f"{saveIndexName}.json"


@task
def load_index(saveIndexLocation):
    saveIndexDict = {}

    if os.path.isfile(saveIndexLocation):
        with open(saveIndexLocation) as inputFile:
            saveIndexDict = json.load(inputFile)
    
    return saveIndexDict


@task
def get_filename_index(saveIndexDict, fileName):
    return saveIndexDict[fileName]


@task
def load_location_file(locationFilePath, logger):
    if os.path.isfile(locationFilePath):
        with open(locationFilePath) as inputFile:
            locations = json.load(inputFile)

        return locations
    
    else:
        logger.warning(f"{locationFilePath} Does not exist. Please run twitterLocations data flow first")
    

@task
def get_placeid(locations, place):
    return locations[place]


@task
def get_trending_subjects(api, placeid):
    return api.trends_place(placeid)[0]['trends']


@task
def clean_trending_subjects(trendingSubjectsRaw):
    return [x['query'] for x in trendingSubjectsRaw]


@task
def get_tweets_trend(trend, api):        
    return list(map(lambda x: x.text, api.search(q=trend, lang='nl', rpp=10)))


@task
def get_all_tweets(trendingSubject, trendingTweets):
    return {trendingSubject: trendingTweets}


@task
def flatten_tweets(trendingTweetsMapped):
    return {k:v for tweets in trendingTweetsMapped for k,v in tweets.items()}


@task
def save_name(fileName, outputLocation, saveIndexDict):
    saveIndexDict[f"{fileName}{date.today()}"] = outputLocation
    return saveIndexDict

@task
def save_index(saveIndexDict, saveIndexLocation):
    with open(saveIndexLocation, 'w') as outputFile:
        json.dump(saveIndexDict, outputFile)


@task
def get_filename(saveLocation, fileName):
    return f"{saveLocation}/{fileName}{date.today()}.json"


@task
def save_data(outputLocation, trendingTweets):
    with open(outputLocation, 'w') as outputFile:
        json.dump(trendingTweets, outputFile)


with Flow("TrendingTweets") as flow:
    # Parameters
    consumerKey = Parameter("consumerKey")
    consumerSecret = Parameter("consumerSecret")
    accessToken = Parameter("accessToken")
    acessTokenSecret = Parameter("acessTokenSecret")
    place = Parameter("place")
    saveIndexName = Parameter("saveIndex")
    saveLocation = Parameter("saveLocation")
    locationFileName = Parameter("locationFileName")
    fileName = Parameter("fileName")

    logger = prefect.context.get("logger")

    # Flow
    api = authenticate_api(consumerKey, consumerSecret, accessToken, acessTokenSecret)
    saveIndexLocation = get_indexname(saveIndexName)

    saveIndexDict = load_index(saveIndexLocation)
    locationFilePath = get_filename_index(saveIndexDict, locationFileName)
    locations = load_location_file(locationFilePath, logger)
    placeid = get_placeid(locations, place)

    trendingSubjectsRaw = get_trending_subjects(api, placeid)
    trendingSubjectsClean = clean_trending_subjects(trendingSubjectsRaw)
    trendingTweetsRaw = get_tweets_trend.map(trend=trendingSubjectsClean, api=unmapped(api))
    trendingTweetsMapped = get_all_tweets.map(trendingSubject=trendingSubjectsClean, trendingTweets=trendingTweetsRaw)
    trendingTweets = flatten_tweets(trendingTweetsMapped)

    outputLocation = get_filename(saveLocation, fileName)
    saveIndexDict = save_name(fileName, outputLocation, saveIndexDict)

    save_index(saveIndexDict, saveIndexLocation)
    save_data(outputLocation, trendingTweets)


def data_pipeline(schedule):
    input = {
        "consumerKey": os.getenv("CONSUMER_KEY"),
        "consumerSecret": os.getenv("CONSUMER_SECRET"),
        "accessToken": os.getenv("ACCESS_TOKEN"),
        "acessTokenSecret":  os.getenv("ACCESS_TOKEN_SECRET"),
        "place": os.getenv("PLACE"),
        "saveIndex":  os.getenv("SAVE_INDEX"),
        "saveLocation":  os.getenv("SAVE_LOCATION"),
        "locationFileName":  os.getenv("TWITTER_COUNTRY_LOCATIONS_OUTPUT"),
        "fileName":  os.getenv("TRENDING_TWEETS_OUTPUT")
    }

    flow.schedule = schedule
    flow.run(parameters=input)


if __name__ == "__main__":
    data_pipeline(schedule)