# Data Versioning Documentation
This handout shows how to apply `Data Versioning` to data saved locally. 

## Prerequisites
- Python version `3.8.6`

## Setup environment
**Optional step**: Create python venv (and activate venv)

```bash
python -m venv env
```

**Step 1**: Update `pip` to the latest version

```bash
python -m pip install -U pip
```

**Step 2**: Create `requirements.txt` with following structure

```.txt
dvc==2.0.6
```

**Step 3**: Install packages from `requirements.txt`

```bash
pip install -r requirements.txt
```

## Steps

### Step 1: Initialize DVC

Only need to perform this step when using DVC for the first time with the given data storage. After that, feel free to skip to step 2.

It's also possible to add a remote storage, by using the `dvc remote add` command. Check the documentation for details. 

```bash
dvc init

dvc add <storage-location>
```

### Step 2: Update Changes

Repeat this step after each change in the data.

```bash
dvc add <storage-location>
```