# Text Analysis Documentation
This handout shows the needed steps you need to take to perform a text analysis on the imported twitter data. 

## Prerequisites
- Python version `3.8.6`
- `.env` file in root, as explained in ReadME. 

## Setup environment
**Optional step**: Create python venv (and activate venv)

```bash
python -m venv env
```

**Step 1**: Update `pip` to the latest version

```bash
python -m pip install -U pip
```

**Step 2**: Create `requirements.txt` with following structure

```.txt
gensim==3.8.3
ipython==7.10.0
json5==0.9.5
matplotlib==3.3.4
prefect==0.14.12
pyLDAvis==3.3.0
python-dotenv==0.15.0
spacy==3.0.5
```

**Step 3**: Install packages from `requirements.txt`

```bash
pip install -r requirements.txt
```

**Step 4**: Install `spacey` language model

```bash
python -m spacy download nl_core_news_sm
```

## Steps 

Needed imports:

```python
import os
from dotenv import load_dotenv
import json
import matplotlib.pyplot as plt
import spacy
import re
from spacy.language import Language
import gensim
import gensim.corpora as corpora
import pyLDAvis
import pyLDAvis.gensim_models as gensimvis
```

### Step 1: Import needed Data

```python
def import_data(name):
    load_dotenv()
    
    with open(f"../{os.getenv('SAVE_INDEX')}") as indexFile:
        saveIndexDict = json.load(indexFile)
        
    outputLocation = f"../{os.getenv('SAVE_LOCATION')}/{saveIndexDict[name]}"
    
    with open(outputLocation) as outputfile:
        output = json.load(outputfile)
        
    return output

importedData = import_data("TrendingTweets2021-03-31")
```

### Step 2: Transform Data

```python
load_dotenv()

nlp = spacy.load({os.getenv('SPACY_NLP_PIPELINE'))

def clean_tweet(tweet):
    cleanedTweet = re.sub(r"\s+", " ", re.sub(r"(\n|#)", " ", re.sub(r"http?s(\S+)", "", re.sub(r"@(\w+)", "", tweet)))).strip()
    return cleanedTweet

sentences = [clean_tweet(sen) for k,v in importedData.items() for sen in v]

@Language.component("stopwords")
def remove_stopwords(doc):
    doc = [token.text for token in doc if token.is_stop != True and token.is_punct != True]
    return doc

nlp.add_pipe("stopwords", last=True)

pipeline = nlp.pipe(sentences, disable=['parser', 'entity_linker', 'entity_ruler', 'attribute_ruler', 'tok2vec', 'transformer'])
transformed_tweets = [sen for sen in pipeline]
```

### Step 3: Generate Insights

```python
# Creates, which is a mapping of word IDs to words.
words = corpora.Dictionary(transformed_tweets)

# Turns each document into a bag of words.
corpus = [words.doc2bow(doc) for doc in transformed_tweets]

# Apply LDA
lda_model = gensim.models.ldamodel.LdaModel(corpus=corpus,
                                           id2word=words,
                                           num_topics=10, 
                                           random_state=2,
                                           update_every=1,
                                           passes=10,
                                           alpha='auto',
                                           per_word_topics=True)

# Create visualization
vis = gensimvis.prepare(lda_model, corpus, words)
pyLDAvis.save_html(vis, 'output.html')
```