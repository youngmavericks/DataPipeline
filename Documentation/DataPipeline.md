# Data Pipeline Documentation
This handout chronicles the steeps needed to construct the data pipeline. 

## Prerequisites
- Python version `3.8.6`
- Twitter developer account authentication keys
- `.env` file in root, as explained in ReadME. 

## Setup environment
**Optional step**: Create python venv (and activate venv)

```bash
python -m venv env
```

**Step 1**: Update `pip` to the latest version

```bash
python -m pip install -U pip
```

**Step 2**: Create `requirements.txt` with following structure

```.txt
json5==0.9.5
python-dotenv==0.15.0
tweepy==3.10.0
```

**Step 3**: Install packages from `requirements.txt`

```bash
pip install -r requirements.txt
```

Now that the environment has been setup, you can start creating the Data Pipeline!

## Steps 

Needed imports:

```python
import os
import tweepy
from dotenv import load_dotenv
from datetime import date 
import json
```

### Step 1: Authenticate Twitter API

```python
def authenticate_api():
    # Load variables from env file into python environment
    load_dotenv()
    
    # Authenticate twitter api
    auth = tweepy.OAuthHandler(os.getenv("CONSUMER_KEY"), os.getenv("CONSUMER_SECRET"))
    auth.set_access_token(os.getenv("ACCESS_TOKEN"), os.getenv("ACCESS_TOKEN_SECRET"))
    
    # Return authenticated API object
    return tweepy.API(auth)

# Get api object
api = authenticate_api()
```

### Step 2: Extract Trending Tweets from Twitter

Get location ID's of twitter locations:

```python
locations = {x['name']: x['woeid'] for x in api.trends_available()}
```

Get trending subjects of twitter, linked to given location:

```python
load_dotenv()
result = api.trends_place(locations[os.getenv("PLACE")])[0]['trends']
trendingSubjects = [x['query'] for x in result]
```

Get tweets linked to trending subjects:

```python
def get_tweets(trend):        
    return list(map(lambda x: x.text, api.search(q=trend, lang='nl', rpp=10)))

trendingTweets = {trend: get_tweets(trend) for trend in trendingSubjects}
```

### Step 3: Load Trending tweets into storage location

```python
def save_name(name, filename):    
    saveIndexDict = {}

    # Check if there already exists an index and import this index if that is the case
    if os.path.isfile(os.getenv("SAVE_INDEX")):
        with open(os.getenv("SAVE_INDEX")) as inputFile:
            saveIndexDict = json.load(inputFile)
    
    saveIndexDict[name] = filename

    with open(os.getenv("SAVE_INDEX"), 'w') as outputFile:
        json.dump(saveIndexDict, outputFile)


def save_json(name, data):    
    outputLocation = f"{os.getenv('SAVE_LOCATION')}/{name}.json"
    save_name(name, outputLocation)
    
    with open(outputLocation, 'w') as outputFile:
        json.dump(data, outputFile)


load_dotenv()

# Save the country Locations
save_json(os.getenv("LOCATIONS"), locations)

# Save trending tweets
save_json(f"{os.getenv('TRENDING_TWEETS')}{date.today()}", trendingTweets)
```