# Data Pipeline
This repo contains an use case for a data pipeline.

## Prerequisites
- `Docker`
- `docker-compose`
- Twitter development account with access tokens

## Usage
Create a `.env` file with the following data (fill in your own twitter access keys):

```.env
CONSUMER_KEY=
CONSUMER_SECRET=
ACCESS_TOKEN=
ACCESS_TOKEN_SECRET=
SAVE_LOCATION=DataLake
SAVE_INDEX=saveIndex
PLACE=Netherlands
TWITTER_COUNTRY_LOCATIONS_OUTPUT=TwitterCountryLocations
TRENDING_TWEETS_OUTPUT=TrendingTweets
SPACY_NLP_PIPELINE=nl_core_news_sm
TWEET_TOPICS_OUTPUT=Output.html
```

Run the following from root:
```
docker-compose up -d
```